---
title: "Installing .Stat Suite from source code"
subtitle: 
comments: false
weight: 40

---
<!-- This page (or a sub-page or sub-section of this page) of the documentation is referenced as an external resource in the .Stat Academy:
* https://academy.siscc.org/installing-dotstat-suite/
Any change affecting its URL must be communicated with the .Stat Academy content admin in advance. -->

The following pages provide examples of topologies (with targetted infrastructures) and the related required .Stat Suite platform components (applications, services, libraries), which can be freely re-used to compose a new topology (system architecture) by anyone on their own infrastructure. Also included are the links to the related instructions for installations starting from the source code.  

This approach requires solid technical knowledge of the underlying ecosystems (.Net, MS SQL Server, JS, OS) but it offers the most flexibility for topologies and configuration.  

You can also have a look at the [Delivery and support streams' diagram](https://sis-cc.gitlab.io/dotstatsuite-documentation/getting-started/) for a better understanding of the installation approach.
