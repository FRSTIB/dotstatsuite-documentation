---
title: "Using .Stat Data Explorer"
subtitle: 
comments: false
weight: 1000

---
<!-- This page (or a sub-page or sub-section of this page) of the documentation is referenced as an external resource in the .Stat Academy:
* https://academy.siscc.org/using-dotstat-suite/
Any change affecting its URL must be communicated to the .Stat Academy content admin in advance. -->

![DE logo](/dotstatsuite-documentation/images/de_logo.png)

* [Design principles & functional vision](/dotstatsuite-documentation/using-de/design-principles/)
* [General layout and common features](/dotstatsuite-documentation/using-de/general-layout/)
* [Searching data](/dotstatsuite-documentation/using-de/searching-data/)
* [Viewing data](/dotstatsuite-documentation/using-de/viewing-data/)
* [SDMX annotations supported by the .Stat Suite](/dotstatsuite-documentation/using-de/sdmx-annotations/)
* [Pop-up survey feature](/dotstatsuite-documentation/using-de/popup-survey/)
