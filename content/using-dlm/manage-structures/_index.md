---
title: "Manage structures"
subtitle: 
comments: false
weight: 232
---

* [Upload structures from an SDMX file (xml)](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/upload-structure/)
* [Delete data structures](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/delete-data-structures/)
* [List related data structures](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/list-related-data-structures/)
* [Copy data structures](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/copy-data-structures/)
* [Edit data structures](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/edit-structure/)
* [Define referential metadata attributes for a data structure](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/link-dsd-msd/)
