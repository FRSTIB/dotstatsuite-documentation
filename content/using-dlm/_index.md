---
title: "Using .Stat Data Lifecycle Manager"
subtitle: 
comments: false
weight: 200

---
<!-- This page (or a sub-page or sub-section of this page) of the documentation is referenced as an external resource in the .Stat Academy:
* https://academy.siscc.org/using-dotstat-suite/
Any change affecting its URL must be communicated to the .Stat Academy content admin in advance. -->

![DLM logo](/dotstatsuite-documentation/images/dlm_logo.png)

* [DLM product design & functional vision](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/product_vision/)
* [Log in the DLM](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/log-in-dlm/)
* [DLM homepage overview](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/dlm_overview/)
* [Manage user access](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-permissions/)
* [Manage structures](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/)
* [Manage data](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-data/)
* [Export all in dump mode](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/dump-mode/)
* [Logbook](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/logs/)
