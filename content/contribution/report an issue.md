---
title: "Report an issue"
subtitle: 
comments: false
weight: 103
---
<!-- This page (or a sub-page or sub-section of this page) of the documentation is referenced as an external resource in the .Stat Academy:
* https://academy.siscc.org/contribute/
Any change affecting its URL must be communicated to the .Stat Academy content admin in advance. -->

### New issue
If you want to report an issue for any of the .Stat Suite applications or services, then you need to **open a new issue** in [Gitlab sis-cc/.stat-suite/issues](https://gitlab.com/groups/sis-cc/.stat-suite/-/issues).  
You can first try to find out if your issue (a bug, a new feature or a feature enhancement) has already been recorded.  

Any new issue needs to be created in a specific .Stat Suite project. The list being quite long, if you don't know to which specific service, tool or application to refer to, then go for the simplest, e.g. *dotstatsuite-data-explorer*, *dotstatsuite-data-lifecycle-manager* or *dotstatsuite-core-common*.  

There are two types of issues you can create: decide whether you want to report a **bug**, or whether you want to suggest a **change** (a new feature or an improvement of an existing behavior).  
For each type, when creating the new issue, be specific in the title, and we will add the related **gitlab label**: *bug* or *feature*.  

If you want to fix a bug yourself, then please refer to the [Development guidelines](https://sis-cc.gitlab.io/dotstatsuite-documentation/contribution/development-guidelines/).  

---

### Writing a good issue
When reporting a **bug**:  
 - Describe what went wrong, what is the expected behavior and if relevant, what caused the problem;
 - If it is not obvious, then list the steps to follow for someone else to reproduce the bug;
 - Screenshots are often very useful to understand the problem;
 - In some cases (server-side errors, API queries, etc.), F12 console details or logs are extremely helpful.

When suggesting a **feature**:  
 - Describe what you would expect to see as a user of the related application (e.g. "as a .Stat Data Explorer user, I want to be able to...");
 - The full behavior and acceptance criteria will help understand the meaning of the feature;
 - Give a clear description of the purpose of the feature, what is the outcome and added-value for the user.
