---
title: "About"
subtitle: 
comments: false
weight: 10
keywords: [
  'License', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/license/',
  'Product overview and flight planner', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/product-overview/',
  'Code of Conduct', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/code-of-conduct/',
  'Licenses of NuGet dependencies of .Stat Core module', https://sis-cc.gitlab.io/dotstatsuite-documentation/about/dotstat-core-nuget-dependency-licenses/',
]
---

* [License](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/license/)
* [Product overview and flight planner](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/product-overview/)
* [Powered by the SIS-CC .Stat Suite](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/powered-by/)
* [Code of Conduct](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/code-of-conduct/)
* [Licenses of NuGet dependencies of .Stat Core module](https://sis-cc.gitlab.io/dotstatsuite-documentation/about/dotstat-core-nuget-dependency-licenses/)
